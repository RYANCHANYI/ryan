﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


using System.Web.Security;
using D1024242052.MyMVC.Models;

namespace D1024242052.MyMVC.Controllers
{
    [AllowAnonymous]
    public class AuthController : Controller
    {
        //
        // GET: /Auth/
        public ActionResult Login(string returnHtml)
        {
            var model = new LoginViewModels()
            {
                ReturnUrl = returnHtml
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult Login(LoginViewModels model)
        {
            if (ModelState.IsValid)
            {
                return View(model);
            }

            if (model.Username == "abc" && model.Password == "123")
            {
                FormsAuthentication.RedirectFromLoginPage(model.Username, false);
                if (model.ReturnUrl == null)
                {
                    model.ReturnUrl = "~/D1024242052/Index";
                }
                return Redirect(model.ReturnUrl);
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult Logout(string returnUrl)
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
    }
}