﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace D1024242052.MyMVC.Models
{
    public class LoginViewModels
    {
        
   
            [Required]
            public string Username { get; set; }
            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }
            [HiddenInput(DisplayValue = false)]
            public string ReturnUrl { get; set; }
        

       
    }
}